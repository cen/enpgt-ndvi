import os
from datetime import datetime
from pathlib import Path

from tools.NdviTool.main import do_ndvi

from write_readme import write_readme

radii = [100, 200, 500, 1000, 2000]

task_file = Path("task.txt")

output_dir = Path("outputs", datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))

if not output_dir.is_dir():
    os.makedirs(output_dir)

do_ndvi(task_file, output_dir, radii)

with open(Path(output_dir, "readme.txt"), "w") as readme_file:
    write_readme(readme_file, radii)
