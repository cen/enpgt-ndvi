from dataclasses import dataclass
from typing import Union

from sentinelhub import BBox
from shapely import MultiPolygon, Polygon


@dataclass(frozen=True)
class SearchArea:
    geo_id: str
    polygon: Union[Polygon, MultiPolygon]


@dataclass(frozen=True)
class RequestDefinition:
    bbox: BBox
    resolution: int


def hash_bbox(bbox: BBox):
    return hash(bbox.max_x) + hash(bbox.max_y) + hash(bbox.min_x) + hash(bbox.min_y) + hash(bbox.crs)


BBox.__hash__ = hash_bbox
