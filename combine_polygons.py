from typing import Iterable, Optional, Tuple

import shapely.ops
from pyproj import Proj, Transformer
from sentinelhub import CRS, BBox, bbox_to_dimensions
from shapely import MultiPolygon
from tqdm import tqdm

from classes import RequestDefinition, SearchArea

WGS_84 = Proj("WGS84")


def get_size(bbox: BBox, resolution: Optional[int] = None) -> Tuple[int, int, int]:
    if resolution is not None:
        size = bbox_to_dimensions(bbox, resolution=resolution)
        return resolution, size[0], size[1]

    auto_resolution = 10

    for auto_resolution in range(10, 70, 10):
        size = bbox_to_dimensions(bbox, resolution=auto_resolution)
        if size[0] * size[1] < 4000:  # 1000-4000, or less if not otherwise available
            break

    return auto_resolution, size[0], size[1]


def combine_polygons(search_areas: Iterable[SearchArea]):
    search_area_set = set(search_areas)

    request_definition_to_search_areas = {}

    with tqdm(total=len(search_area_set), desc="Combining polygons to make fewer requests") as pbar:
        while search_area_set:
            current_search_area_set_size = len(search_area_set)

            next_search_area = search_area_set.pop()
            next_polygon = next_search_area.polygon

            original_bbox = BBox(bbox=next_polygon.bounds, crs=CRS.WGS84)
            resolution, x_size, y_size = get_size(original_bbox)

            associated_search_areas = [next_search_area]
            associated_polygons = [next_polygon]
            current_multipolygon = MultiPolygon([search_area.polygon for search_area in associated_search_areas])
            current_multipolygon_hull = current_multipolygon.convex_hull

            for search_area in search_area_set:
                candidate_polygon = search_area.polygon

                if not candidate_polygon.intersects(current_multipolygon_hull):
                    continue

                # Don't add a polygon that would want a better resolution and accidentally mess up its resolution
                # Also make sure they're not too far apart, even if the candidate resolution is worse
                candidate_bbox = BBox(bbox=candidate_polygon.bounds, crs=CRS.WGS84)
                candidate_resolution, _, _ = get_size(candidate_bbox)
                if candidate_resolution < resolution or candidate_resolution > resolution + 10:
                    continue

                next_associated_polygons = [*associated_polygons, candidate_polygon]
                next_multipolygon = MultiPolygon(next_associated_polygons)
                new_bbox = BBox(bbox=next_multipolygon.bounds, crs=CRS.WGS84)

                _, new_x, new_y = get_size(new_bbox, resolution=resolution)

                # Only expand bbox up to 4 times original size
                if new_x * new_y / (x_size * y_size) > 4:
                    continue

                associated_search_areas.append(search_area)
                associated_polygons = next_associated_polygons
                current_multipolygon = next_multipolygon
                current_multipolygon_hull = current_multipolygon.convex_hull

            search_area_set -= set(associated_search_areas)

            # Multipolygon -> Bounding Box

            lng, lat = current_multipolygon_hull.centroid.xy

            crs_aeqd = Proj(proj="aeqd", datum="WGS84", lon_0=lng[0], lat_0=lat[0], units="m")
            transproj = Transformer.from_proj(crs_aeqd, WGS_84, always_xy=True)
            reverse_transproj = Transformer.from_proj(WGS_84, crs_aeqd, always_xy=True)

            hull_local = shapely.ops.transform(reverse_transproj.transform, current_multipolygon_hull)
            buffer_poly_local = hull_local.buffer(resolution * 2)
            buffer_poly_wgs84 = shapely.ops.transform(transproj.transform, buffer_poly_local)

            definition = RequestDefinition(BBox(buffer_poly_wgs84.bounds, CRS.WGS84), resolution)
            request_definition_to_search_areas[definition] = associated_search_areas

            pbar.update(current_search_area_set_size - len(search_area_set))

    return request_definition_to_search_areas
