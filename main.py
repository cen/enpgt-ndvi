import ast
import os
import sys
from datetime import datetime
from functools import lru_cache
from pathlib import Path
from typing import List

import numpy
from enpgt_task_file_reader import convert_nsv_task_file_to_polygons
from geotiff_extractor.geotiff_extraction import general_mode
from geotiff_extractor.results_dataclasses import SingleTifResult
from geotiff_extractor.tif_dataclasses import GeoTiffData
from sentinelhub import (
    DataCollection,
    MimeType,
    MosaickingOrder,
    SentinelHubRequest,
    SHConfig,
    bbox_to_dimensions,
)
from tqdm import tqdm

from classes import RequestDefinition, SearchArea
from combine_polygons import WGS_84, combine_polygons
from write_readme import write_readme

config = SHConfig()


def make_config() -> None:
    from cdse_client_details import client_id, client_secret

    config.sh_client_id = client_id
    config.sh_client_secret = client_secret
    config.sh_base_url = "https://sh.dataspace.copernicus.eu"
    config.sh_token_url = "https://identity.dataspace.copernicus.eu/auth/realms/CDSE/protocol/openid-connect/token"
    config.save()


BUFFER_RADIUS = 50


@lru_cache(maxsize=1)
def request_nvdi(request_definition: RequestDefinition) -> numpy.ndarray[numpy.float32]:
    size = bbox_to_dimensions(request_definition.bbox, resolution=request_definition.resolution)

    evalscript_true_color = """
    //VERSION=3
    function setup() {
      return{
        input: [{
          bands: ["B04", "B08"]
        }],
        output: {
          id: "default",
          bands: 1,
          sampleType: SampleType.FLOAT32,
        }
      }
    }
    function evaluatePixel(sample) {
      let ndvi = (sample.B08 - sample.B04) / (sample.B08 + sample.B04)
      return [ ndvi ]
    }
    """

    nvdi_request = SentinelHubRequest(
        evalscript=evalscript_true_color,
        input_data=[
            SentinelHubRequest.input_data(
                data_collection=DataCollection.SENTINEL2_L1C.define_from("s2l1c", service_url=config.sh_base_url),
                time_interval=("2022-06-01", "2022-06-30"),
                mosaicking_order=MosaickingOrder.LEAST_CC,
            )
        ],
        responses=[SentinelHubRequest.output_response("default", MimeType.TIFF)],
        bbox=request_definition.bbox,
        size=size,
        config=config,
    )

    return nvdi_request.get_data()[0]


def do_single_ndvi(request_definition: RequestDefinition, search_area: SearchArea) -> SingleTifResult:
    area_poly_wgs84 = search_area.polygon

    bbox = request_definition.bbox

    image = request_nvdi(request_definition)
    size = image.shape

    pixel_width = (bbox.max_x - bbox.min_x) / size[1]
    pixel_height = (bbox.max_y - bbox.min_y) / size[0]

    fake_geotiff = GeoTiffData(
        "Fakepath",  # (No real path required for the function we're calling)
        image,  # (This is meant to be a zarray, but in this case we pass a numpy array since we already have that)
        pixel_width,
        pixel_height,
        bbox.min_x + pixel_width * 0.5,
        bbox.max_y - pixel_height * 0.5,
        bbox.max_x - pixel_width * 0.5,
        bbox.min_y + pixel_height * 0.5,
        WGS_84,
        image.dtype,
        None,
    )

    return general_mode(search_area.geo_id, fake_geotiff, area_poly_wgs84)


def do_ndvi(task_file: Path, output_dir: Path, radii: List[int]):
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    all_geo_ids_to_all_polygons = {}
    all_geo_ids = []

    for radius in radii:
        geo_id_to_polygon = convert_nsv_task_file_to_polygons(task_file, radius)
        if not all_geo_ids:
            all_geo_ids.extend(geo_id_to_polygon)

        all_geo_ids_to_all_polygons.update(
            {geo_id + f"_NDVITool_Radius{radius}": polygon for geo_id, polygon in geo_id_to_polygon.items()}
        )

    search_areas = [SearchArea(geo_id, polygon) for geo_id, polygon in all_geo_ids_to_all_polygons.items()]

    request_definition_to_search_areas = combine_polygons(search_areas)

    outputs: List[SingleTifResult] = []
    for request_definition, search_areas in tqdm(request_definition_to_search_areas.items(), desc="Calculating NDVI"):
        outputs += [do_single_ndvi(request_definition, search_area) for search_area in search_areas]

    outputs_by_radius = {radius: {} for radius in radii}

    for output in outputs:
        geo_id, radius = tuple(output.geo_id.split("_NDVITool_Radius"))

        outputs_by_radius[int(radius)][geo_id] = output

    for radius in radii:
        with open(output_dir / f"output_{radius}.csv", "w") as output_file:
            output_file.write(",".join(SingleTifResult.all_field_names()) + "\n")

            for geo_id in all_geo_ids:
                single_tif_result = outputs_by_radius[radius][geo_id]
                output_file.write(f"{geo_id},{','.join(list(single_tif_result.as_str_dict().values())[1:])}\n")


def main(args: List[str]) -> int:
    default_task_file = "task.txt"

    radii = [100, 200, 500, 1000, 2000]

    for arg in args:
        if arg.startswith("[") and arg.endswith("]"):
            radii = ast.literal_eval(arg)

        elif arg.isnumeric():
            radii = [int(arg)]

        elif os.path.isfile(arg):
            default_task_file = arg

        else:
            return -1

    if not os.path.isfile(default_task_file):
        return -1

    task_file = Path(default_task_file)
    output_dir = Path("outputs", datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))

    make_config()
    do_ndvi(task_file, output_dir, radii)

    with open(output_dir / "readme.txt", "w") as f:
        write_readme(f, radii)

    return 0


if __name__ == "__main__":
    main(sys.argv[1:])
