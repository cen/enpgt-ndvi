from typing import List, TextIO


def write_readme(f: TextIO, radii: List[int]):
    f.write(
        "Normalized Difference Vegetation Index Tool\n\n"
        "This file contains some information on how the data was generated and how to interpret it."
    )

    f.write(
        "\n\n-----\n\n"
        "Normalized Difference Vegetation Index Dataset\n\n"
        "This tool uses satellite images from Sentinel 2 to"
        " calculate the Normalized Difference Vegetation Index (NDVI).\n"
        "NDVI is a metric for density and health of vegetation that is calculated from the red and near-infrared"
        " channels of satellite imagery.\n\n"
        "You can read up on Sentinel 2 here:\n"
        "https://www.esa.int/Applications/Observing_the_Earth/Copernicus/Sentinel-2\n\n"
        "Here's the wikipedia entry for the NDVI:\n"
        "https://en.wikipedia.org/wiki/Normalized_difference_vegetation_index\n"
    )

    f.write(
        "\n-----\n\n"
        "INPUT AND OUTPUT\n\n"
        "Each line in the task file defines a location.\n"
        "The NDVI Tool will extract and average out the NDVI values in a radius around each location.\n\n"
    )

    radius_text = f'In this case, the radius {radii[0]}m was used. The output can be viewed in "output_{radii[0]}.csv".'
    if len(radii) > 1:
        radius_text = (
            f"In this case, the args.radii {', '.join(str(radius) for radius in radii[:-1])}"
            f" and {radii[-1]} were used. For each radius, you'll be able to find an output file, for"
            f' example: "output_{radii[0]}.csv".'
        )

    f.write(radius_text + "\n")

    f.write(
        "An output file usually contains the id of each location, followed by the NDVI values calculated by the tool."
    )

    f.write("\n\n-----\n\nWHAT EXACTLY WAS DONE FOR THIS TASK?\n\n")

    f.write(
        "To get the NDVI for a location, a request to the Sentinel API needs to be made for a satellite image.\n"
        "This API request is made using the Copernicus Data Space Ecosystem API as a middleman\n."
        "A few preparation steps are done to ensure we limit the size and number of our requests.\n\n"
        "1. Make sure we are using an appropriate resolution.\n"
        "Sentinel offers resolutions 10m*10m up to 60m*60m.\n"
        "For a 2000m radius, we don't need to use 10m resolution, it would simply be overkill.\n\n"
        "2. Combine nearby locations into one request.\n"
        "If two locations / search areas are near each other, and their resolutions roughly match, we can do"
        " use one satellite image for both of them.\n"
        "The algorithm for this is not very aggressive, because we do not want weird correlations - It should only be"
        " done if everything matches.\n\n"
        "Sentinel also provides methods to get satellite images with the lowest possible cloud cover.\n"
        "The NDVI tool will ask Sentinel to use the lowest cloud cover pictures within a specified month, and stitch"
        " them together in a mosaic-like fashion.\n\n"
        "For each pixel in the satellite image, the NDVI is calculated as (NIR - Red)/(NIR + Red).\n"
        "Then, all the pixels in a radius around a location are collected, and the arithmetic mean and standard"
        " deviation are calculated for the NDVI.\n\n"
        "If a pixel square only partially falls within the radius"
        ", it is only partially considered. (weighted mean & stddev by area within the radius)\n\n"
    )

    f.write("\n-----\n\nOUTPUT\n\n")

    f.write(
        "Each output has the following parameters:\n\n"
        "geo_id - Unique identifier for location.\n"
        "pixel_amount - Amount of pixel squares that fell within the area.\n"
        "used_pixel_amount - Amount of pixel squares that were actually used.\n"
        "invalid_pixel_amount - Amount of pixel squares that were discarded due to having an invalid value.\n"
        "resolution - The actual resolution of the pixel squares in m².\n"
        "mean - Weighted-by-area mean of NDVI values that fall within the radius.\n"
        "stddev - Weighted-by-area standard deviation of NDVI values that fall within the radius.\n"
        "sum - Sum of pixel values in the area.\n"
        "area - Total area of pixel squares that fell within the radius.\n"
        "used_area - Total area of pixel squares that fell within the radius,"
        " minus those that had invalid values.\n"
    )
